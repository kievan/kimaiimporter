<?php

require_once(__DIR__ . '/config.php');
require_once(__DIR__ . '/Office365.php');
require_once(__DIR__ . '/Kimai.php');
require_once(__DIR__ . '/Jira.php');

date_default_timezone_set($config['timezone']);

$shortopts = '';

$longopts  = array(
    "s:", // --s - startDayOffset (required value)
    "e:", // --e - endDayOffset (required value)
);
$options = getopt($shortopts, $longopts);
$startDayOffset = $config['startDayOffset'];
$endDayOffset   = $config['endDayOffset'];
if(!empty($options)){
    if(isset($options['s']) && is_numeric($options['s'])) {
        $startDayOffset = $options['s'] . ' day';
    } else if(isset($options['s']) && !is_numeric($options['s'])) {
        $startDayOffset = $options['s'];
    }
    if(isset($options['e']) && is_numeric($options['e'])){
        $endDayOffset = $options['e'] . ' day';
    } else if (isset($options['e']) && !is_numeric($options['e'])) {
        $endDayOffset = $options['e'];
    }

    if((int)$startDayOffset > (int)$endDayOffset){
        echo sprintf("Start: %s, End: %s".PHP_EOL, (int)$startDayOffset, (int)$endDayOffset);
        colorize_output('Start day offset MUST be smaller than end day offset. Exiting... ', '✗', '38;5;160');
        exit;
    }
}

$office365 = new Office365($config['office365']['shared.calendar.url']);
$startDatetime =
    (new DateTime())
        ->modify($startDayOffset)
        ->format('Y-m-d\T00:01+03:00'); // format to the start of day
$endDatetime =
    (new DateTime())
        ->modify($endDayOffset)
        ->format('Y-m-d\T23:59+03:00'); // format to the end of day
$params = [
    'startDatetime' => $startDatetime,
    'endDatetime'   => $endDatetime,
];

colorize_output('Working... ', '⇩', '38;5;226');
$calendarEventsJson = $office365->getCalendarEvents($params);

// For testing
$calendarEvents = json_decode($calendarEventsJson, true);

// Get Office 365 events
$office365Events = $calendarEvents['Body']['ResponseMessages']['Items'][0]['RootFolder']['Items'];
colorize_output('Office365 event id count: ', count($office365Events), '38;5;226');

// Initilize events which will be imported into Kimai
$daysWithEvents = [];


/*
 * Form list of events for the selected datetime range
 */
foreach ($office365Events as $event) {

    colorize_output('Getting Office 365 expanded event:', '.', '38;5;226');
    $jsonArr = json_decode($office365->getFullCalendarEvent(['itemId' => $event['ItemId']['Id']]), true);
    $eventHtml = strip_tags($jsonArr["Body"]["ResponseMessages"]["Items"][0]["Items"][0]["Body"]["Value"]);

    preg_match('/[Kk][Ii][Mm][Aa][Ii]:\s*([a-zA-Z\s_]*)[\-\/\s]*([a-zA-Z\s_]*)/', $eventHtml, $kimaiProjectActivityMatch);

    if(empty($kimaiProjectActivityMatch)){
        $subject = strtolower($jsonArr["Body"]["ResponseMessages"]["Items"][0]["Items"][0]["Subject"]);
        $kimai_project = trim($subject);
        $kimai_activity = 'Project_Work';
    } else {
        $kimai_project = trim($kimaiProjectActivityMatch[1]);
        $kimai_activity = empty($kimaiProjectActivityMatch[2]) ? 'Project_Work' : trim($kimaiProjectActivityMatch[2]);
    }

    echo 'Project: ' . $kimai_project . PHP_EOL;
    echo 'Activity: ' . $kimai_activity . PHP_EOL;

    $free = $event['FreeBusyType'] === 'Free' ? 1 : 0;
    $startDateTime = new Datetime($event['Start']);
    $endDateTime = new DateTime($event['End']);
    $start_day = $startDateTime->format('d.m.Y');
    $end_day = $endDateTime->format('d.m.Y');
    $start_time = $startDateTime->format('H:i:s');
    $end_time = $endDateTime->format('H:i:s');

    $duration = $startDateTime->diff($endDateTime)->format("%H:%I:%S");
    $daysWithEvents[$start_day][] = [
        'description'    => $event['Subject'],
        'start_day'      => $start_day,
        'end_day'        => $end_day,
        'start_time'     => $start_time,
        'end_time'       => $end_time,
        'duration'       => $duration,
        'free'           => $free,
        'kimai_project'  => $kimai_project,
        'kimai_activity' => $kimai_activity,
    ];
}


/**
 * Add Kimai entries
 */
$kimai = new Kimai($config['kimai']);

foreach($daysWithEvents as $date => $o365Events) {
    echo PHP_EOL . "===== Date: $date =====" . PHP_EOL;

    $jira = new Jira($config['jira']);
    $jiraStartDay = (new DateTime($o365Events[0]['start_day']))->modify('midnight')->modify('1 minute')->format('Y-m-d H:i');
    $jiraEndDay = (new DateTime($o365Events[0]['end_day']))->modify('tomorrow')->modify('1 minute ago')->format('Y-m-d H:i');
    $params = ['startDate' => $jiraStartDay, 'endDate' => $jiraEndDay, ];
    $result = json_decode($jira->getUserIssuesByDateRange($params), true);

    // TODO: instead of relying on concrete field key, grep for $projectName somehow
    $projectName = trim(strtolower(preg_replace('/[\ \/]/', '_', $result['issues'][0]['fields']['customfield_11752']['value'])));
    $projectName = (empty($projectName)) ? "6s_others" : $projectName;

    // TODO: create activity name to id matching
    $activityName = 'project_work';

    $projectActivityIds = $jira->getProjectAndActivityIds($projectName, $activityName);
    $description = $result['issues'][0]['key']
        . ' - '
        . $result['issues'][0]['fields']['summary']
        . ' -> implementation';
    $jiraIssue = [
        'description' => $description,
        'projectId'   => $projectActivityIds[$projectName],
        'activityId'  => $projectActivityIds[$activityName],
    ];

    // TODO: subtract lunch time slots from tracked time
    $jiraEvents = $jira->processDayEvents($jiraIssue, $o365Events, $config);

    addEventsToKimai($kimai, $o365Events);
    addEventsToKimai($kimai, $jiraEvents);

    echo "===== Date: $date =====" . PHP_EOL;
}

/**
 * @param Kimai $kimai
 * @param array $events
 */
function addEventsToKimai($kimai, $events){

    foreach($events as $event) {
        $message = "{$event['description']} \n ({$event['start_time']} - {$event['end_time']}): ";
        $errors  = $kimai->addEntry($event);
        if(empty($errors['errors'])) {
            colorize_output($message, '✓', '38;5;112');
        } else {
            colorize_output($message, '✗', '38;5;160');
            echo json_encode($errors);
        }
    }

}
function colorize_output($message, $coloredText, $color){
    echo shell_exec("echo '\e[38;5;255m {$message} \e[39m \e[{$color}m $coloredText \e[39m'");
}
