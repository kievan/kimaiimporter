<?php


class Office365 {

    private $publicCalendarUrl = '';

    // Result of investigation, no documentation could be found at the time of writing
    private $calendarServiceName = 'service.svc';
    private $serviceUrl;

    private $requestHeaders = [];
    private $requestPayload = '';


    /**
     * Office365 constructor.
     *
     * @param string $calendarUrl
     */
    function __construct($calendarUrl){

        $this->publicCalendarUrl = $calendarUrl;

        // Transform public calendar url into service endpoint
        $segments = parse_url($calendarUrl);
        $pathParts = explode('/',$segments['path']);
        array_pop($pathParts);
        $pathParts[] = $this->calendarServiceName;
        $segments['path'] = implode('/', $pathParts);
        $newCalendarUrl = $this->_buildUrl($segments);

        $this->serviceUrl = $newCalendarUrl;
    }


    /**
     * Get calendar events for a specified datetime range
     *
     * @param array $params
     *
     * @internal string startDatetime
     *           string endDatetime
     * @return array
     */
    public function getCalendarEvents(array $params) {

        $params['apiMethodName'] = 'FindItem';
        $this->_setRequestHeaders('FindItem');
        $this->_setRequestPayload($params);

        return $this->getData();
    }


    /**
     * Get calendar events for a specified datetime range
     *
     * @param array $params
     *
     * @internal string itemId        - event item id
     *
     * @return string
     */
    public function getFullCalendarEvent(array $params) {

        $params['apiMethodName'] = 'GetItem';
        $this->_setRequestHeaders('GetItem');
        $this->_setRequestPayload($params);

        return $this->getData();
    }


    /**
     * Get json from Office365 server
     *
     * @return mixed
     */
    protected function getData(){

        $ch = curl_init($this->serviceUrl);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->requestHeaders);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $this->requestPayload);

        $result = curl_exec($ch);

        return $result;
    }


    /**
     * Result of investigation, no documentation could be found at the time of writing
     * Not sure what these headers are for:
     * 'AC: 1'
     * 'X-OWA-ActionId: 1' or 'X-OWA-ActionId: -3'
     * But it is clear that they are needed.
     *
     * @param string $apiMethodName
     */
    private function _setRequestHeaders($apiMethodName){

        // Perform some version randomization, just in case
        $userAgentRandVersions =
            'Mozilla/5.0 '
            . '(X11; Linux x86_64) '
            . 'AppleWebKit/53%d.%d '
            . '(KHTML, like Gecko) '
            . 'Chrome/5%d.%d.%d.%d '
            . 'Safari/53%d.%d';

        $appleWebKit = [
            mt_rand(0,9), mt_rand(10,99)
        ];
        $userAgent = sprintf(
            $userAgentRandVersions,
            $appleWebKit[0], $appleWebKit[1],
            mt_rand(0,9), mt_rand(0,9), mt_rand(1000, 2785), mt_rand(100,999),
            $appleWebKit[0], $appleWebKit[1]
        );

        switch($apiMethodName){
            case 'FindItem':
                $this->requestHeaders = [
                    'AC: 1',
                    'X-OWA-ActionId: 1',
                    'Action: FindItem',
                    'X-OWA-ActionName: GetCalendarItemsAction_Day',
                    'Content-Type: application/json; charset=UTF-8',
                    'User-Agent: ' . $userAgent
                ];

                break;

            case 'GetItem':
                $this->requestHeaders = [
                    'AC: 1',
                    'X-OWA-ActionId: -3',
                    'Action: GetItem',
                    'X-OWA-ActionName: GetCalendarItem',
                    'Content-Type: application/json; charset=UTF-8',
                    'User-Agent: ' . $userAgent
                ];

                break;

            default:
                $this->requestHeaders = [];

                break;
        }
    }


    /**
     * Result of investigation, no documentation could be found at the time of writing
     *
     * @param array $params
     *
     * @internal string apiMethodName == FindItem
     *             string startDatetime
     *             string endDatetime
     *           string apiMethodName == GetItem
     *              string - itemId
     */
    private function _setRequestPayload(array $params){

        $apiMethodName = $params['apiMethodName'];

        switch($apiMethodName){
            case 'FindItem':
                $requestPayload = <<< PAYLOAD
                    {
                      "__type": "FindItemJsonRequest:#Exchange",
                      "Header": {
                        "__type": "JsonRequestHeaders:#Exchange",
                        "RequestServerVersion": "Exchange2013",
                        "TimeZoneContext": {
                          "__type": "TimeZoneContext:#Exchange",
                          "TimeZoneDefinition": {
                            "__type": "TimeZoneDefinitionType:#Exchange",
                            "Id": "FLE Standard Time"
                          }
                        }
                      },
                      "Body": {
                        "__type": "FindItemRequest:#Exchange",
                        "ItemShape": {
                          "__type": "ItemResponseShape:#Exchange",
                          "BaseShape": "IdOnly"
                        },
                        "ParentFolderIds": [
                          {
                            "__type": "FolderId:#Exchange",
                            "Id": "AAMkADUzZDllYzFkLTQyYTAtNDdkZC1hMjZjLTg4NmYyZGVlMTk0YQAuAAAAAADJ/9MNIvi8T7dgNAfx27NkAQBvn+ScaNq6QpFpiAhHcNOAAAAAAAENAAA=",
                            "ChangeKey": "AgAAAA=="
                          }
                        ],
                        "Traversal": "Shallow",
                        "Paging": {
                          "__type": "CalendarPageView:#Exchange",
                          "StartDate": "2016-10-06T00:00:00+03:00",
                          "EndDate": "2016-10-07T00:00:00+03:00"
                        }
                      }
                    }
PAYLOAD;

                $requestPayloadArr = json_decode($requestPayload, true);
                $requestPayloadArr['Body']['Paging']['StartDate'] = $params['startDatetime'];
                $requestPayloadArr['Body']['Paging']['EndDate'] = $params['endDatetime'];
                $this->requestPayload = json_encode($requestPayloadArr);

                break;

            case 'GetItem':
                $requestPayload = <<< PAYLOAD
                    {
                      "__type": "GetItemJsonRequest:#Exchange",
                      "Header": {
                        "__type": "JsonRequestHeaders:#Exchange",
                        "RequestServerVersion": "V2_75",
                        "TimeZoneContext": {
                          "__type": "TimeZoneContext:#Exchange",
                          "TimeZoneDefinition": {
                            "__type": "TimeZoneDefinitionType:#Exchange",
                            "Id": "FLE Standard Time"
                          }
                        }
                      },
                      "Body": {
                        "__type": "GetItemRequest:#Exchange",
                        "ItemShape": {
                          "__type": "ItemResponseShape:#Exchange",
                          "BaseShape": "IdOnly",
                          "FilterHtmlContent": true,
                          "BlockExternalImagesIfSenderUntrusted": true,
                          "AddBlankTargetToLinks": true,
                          "ClientSupportsIrm": true,
                          "InlineImageUrlTemplate": "service.svc/s/GetFileAttachment?id={id}&X-OWA-CANARY=jiYNL8tEhEGXVZzN7jiBiPDfgJsW_NMYpn2qBIRraFGTj-C4xaYr346Q15wzStE3BqGDauZJ1Iw.",
                          "FilterInlineSafetyTips": true,
                          "MaximumBodySize": 0,
                          "CssScopeClassName": "rps_f39b",
                          "BodyType": "HTML"
                        },
                        "ItemIds": [
                          {
                            "__type": "ItemId:#Exchange",
                            "Id": "AAMkADUzZDllYzFkLTQyYTAtNDdkZC1hMjZjLTg4NmYyZGVlMTk0YQFRAAgI0\/ugsqcAAEYAAAAAyf\/TDSL4vE+3YDQH8duzZAcAb5\/knGjaukKRaYgIR3DTgAAAAAABDQAAb5\/knGjaukKRaYgIR3DTgAAABYDdtQAAEA=="
                          }
                        ],
                        "ShapeName": "FullCalendarItem"
                      }
                    }
PAYLOAD;
                $requestPayloadArr = json_decode($requestPayload, true);
                $requestPayloadArr['Body']['ItemIds'][0]['Id'] = $params['itemId'];
                $this->requestPayload = json_encode($requestPayloadArr, JSON_PRETTY_PRINT);
//                var_dump($this->requestPayload);exit(PHP_EOL.'requestPayload GetItem'.PHP_EOL);

                break;

            default:
                $this->requestPayload = json_encode([]);
        }

    }


    /**
     * Build url from parts of parse_url()
     *
     * @param array $parts
     *
     * @return string
     */
    private function _buildUrl(array $parts) {

        return (isset($parts['scheme']) ? "{$parts['scheme']}:" : '') .
            ((isset($parts['user']) || isset($parts['host'])) ? '//' : '') .
            (isset($parts['user']) ? "{$parts['user']}" : '') .
            (isset($parts['pass']) ? ":{$parts['pass']}" : '') .
            (isset($parts['user']) ? '@' : '') .
            (isset($parts['host']) ? "{$parts['host']}" : '') .
            (isset($parts['port']) ? ":{$parts['port']}" : '') .
            (isset($parts['path']) ? "{$parts['path']}" : '') .
            (isset($parts['query']) ? "?{$parts['query']}" : '') .
            (isset($parts['fragment']) ? "#{$parts['fragment']}" : '');
    }
}