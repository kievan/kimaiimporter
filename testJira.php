<?php
require_once(__DIR__ . '/config.php');
require_once(__DIR__ . '/Jira.php');

//$jira = new Jira($config['jira']);
//$params = ['startDate' => '2016-11-14', '2016-11-14'];
//$result = json_decode($jira->getIssuesAssignedToUser($params), true);

$result = json_decode(file_get_contents('testJira.log'), true);
$resultSnipped = [
    'name' => $result['issues'][0]['key'] . ' ' . $result['issues'][0]['fields']['summary'],
    'type' => $result['issues'][0]['fields']['customfield_10872']['value'],
];

print_r($resultSnipped);
//file_put_contents('testJira.log',json_encode($resultSnipped, JSON_PRETTY_PRINT));

// Test
//$testEvent = [
//    'description'    => 'Dragons - Stand uup',
//    'start_day'      => '17.10.2016',
//    'end_day'        => '17.10.2016',
//    'start_time'     => '10:45:00',
//    'end_time'       => '11:00',
//    'duration'       => '00:15:00',
//    'free'           => 0,
//    'kimai_project'  => 'Scrum_Meetings',
//    'kimai_activity' => 'Project_Work',
//];

$events = [
    [
        'description'    => 'Dragons - Stand up',
        'start_day'      => '17.10.2016',
        'end_day'        => '17.10.2016',
        'start_time'     => '10:45:00',
        'end_time'       => '11:00',
        'duration'       => '00:15:00',
        'free'           => 0,
        'kimai_project'  => 'Scrum_Meetings',
        'kimai_activity' => 'Project_Work',
    ],
    [
        'description'    => 'Lunch',
        'start_day'      => '17.10.2016',
        'end_day'        => '17.10.2016',
        'start_time'     => '12:00',
        'end_time'       => '13:00',
        'duration'       => '01:00:00',
        'free'           => 0,
        'kimai_project'  => 'Scrum_Meetings',
        'kimai_activity' => 'Project_Work',
    ],

    [
        'description'    => 'Dragons - Stand down',
        'start_day'      => '17.10.2016',
        'end_day'        => '17.10.2016',
        'start_time'     => '13:00',
        'end_time'       => '13:05',
        'duration'       => '01:00:00',
        'free'           => 0,
        'kimai_project'  => 'Scrum_Meetings',
        'kimai_activity' => 'Project_Work',
    ],

    [
        'description'    => 'Dragons - Stand down',
        'start_day'      => '17.10.2016',
        'end_day'        => '17.10.2016',
        'start_time'     => '13:20',
        'end_time'       => '14:00',
        'duration'       => '01:00:00',
        'free'           => 0,
        'kimai_project'  => 'Scrum_Meetings',
        'kimai_activity' => 'Project_Work',
    ],


    [
        'description'    => 'Dragons - Stand left',
        'start_day'      => '17.10.2016',
        'end_day'        => '17.10.2016',
        'start_time'     => '15:00',
        'end_time'       => '16:00',
        'duration'       => '00:15:00',
        'free'           => 0,
        'kimai_project'  => 'Scrum_Meetings',
        'kimai_activity' => 'Project_Work',

    ],

];
// Get array keys
$arrayKeys = array_keys($events);
// Fetch first array key
$firstArrayKey = array_shift($arrayKeys);
// Fetch last array key
$lastArrayKey = array_pop($arrayKeys);
//iterate array

$jiraEvents = [];

$jiraEventTimes = [];
$arrayLength = count($events);
for($k = 0; $k < $arrayLength; $k++){
    $event = [];
    $event1 = [];
    $addiotionalFields = [
        'description' => $resultSnipped['name'],
        'start_day' => $events[$k]['start_day'],
        'end_day' => $events[$k]['end_day'],
        'free' => 0,
        'kimai_project'  => 'Get from Jira',
        'kimai_activity' => 'Get from Jira',
    ];

    if($k == $firstArrayKey) {

        $start_time = $config['startOfDay'];
        $end_time   = $events[$k]['start_time'];

        $event += addJiraTime($start_time, $end_time);
        $event += $addiotionalFields;

    } else {

        $start_time = $events[$k-1]['end_time'];
        $end_time   = $events[$k]['start_time'];

        $jiraTime = addJiraTime($start_time, $end_time);
        if(!empty($jiraTime)) {
            $event1 += $jiraTime;
            $event1 += $addiotionalFields;
        }
    }

    if($k == $lastArrayKey) {

        $start_time = $events[$k]['end_time'];
        $end_time   = $config['endOfDay'];

        $event += addJiraTime($start_time, $end_time);
        $event += $addiotionalFields;
    }

    if(!empty($event1))
        $jiraEventTimes[] = $event1;
    if(!empty($event))
        $jiraEventTimes[] = $event;
}

function addJiraTime($start_time, $end_time) {
    $arr = [];
    $startTime = new DateTime($start_time);
    $endTime = new DateTime($end_time);
    //                $start_time = $startDateTime->format('H:i:s');
    //                $end_time = $endDateTime->format('H:i:s');
    //
    $duration = $startTime->diff($endTime)->format("%H:%I:%S");

    $timesEqual = ($startTime == $endTime);
    if(!$timesEqual){
        return [
            'start_time' => $start_time,
            'end_time' => $end_time,
            'duration' => $duration,
        ];

        //        print_r(PHP_EOL . 'DUR: '.$duration . PHP_EOL);

    } else {
        return [];
    }

    //    exit;
}

print_r($jiraEventTimes);