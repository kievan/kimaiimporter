<?php

return $config = [
    // GLOBAL
    'timezone' => 'Europe/Kiev',
    // How many days back to start searching for events
    // Should be equal or less than endDayOffset
    'startDayOffset' => '-1 day',
    // How many days back to end searching for events
    // Should be equal or more than startDayOffset
    'endDayOffset' => '-1 day',
    // Start of day
    'startOfDay' => '9:00',
    // End of day
    'endOfDay' => '18:00',
    // Duration of lunch
    'durationOfLunch' => '1:00',
    //lunchStartTime
    'lunchStartTime' => '12:30',
    //lunchEndTime
    'lunchEndtime'  => '13:30',

    // Service specific
    'office365' => [
        // Your Office365 shared calendar url
        'shared.calendar.url' => '',
    ],
    'jira'      => [
        // ADD YOUR CREDENTIALS
        // https://developer.atlassian.com/jiradev/jira-apis/jira-rest-apis/jira-rest-api-tutorials/jira-rest-api-example-basic-authentication
        'username'       => '',
        'password'       => '',

        // DO NOT CHANGE
        // https://developer.atlassian.com/jiradev/jira-apis/jira-rest-apis/jira-rest-api-tutorials/jira-rest-api-example-query-issues
        'api.url'        => 'https://pm.sixt.com/rest/api/2/search',
        'api.url.params' => [
//            'jql' => 'assignee={{jira.username}}'
//            'jql' => 'assignee was currentUser() DURING ("{{startDate}}","{{endDate}}") AND status WAS IN ("Active") and key = "RACMETRO-14211" order by key asc',
//            'jql' => 'assignee was currentUser() DURING ("{{startDate}}","{{endDate}}") AND status WAS IN ("Active") order by key asc',
//            'jql' => 'assignee was currentUser() DURING ("{{startDate}}","{{endDate}}") AND status WAS IN ("Active") and type = bug',
//            'jql' => 'assignee was currentUser() DURING ("{{startDate}}","{{endDate}}") AND status WAS IN ("Active")',
            'jql' => 'assignee was currentUser() DURING ("{{startDate}}","{{endDate}}") AND (status WAS IN ("Active")  DURING ("{{startDate}}","{{endDate}}")) ORDER BY updated asc
            ',

        ]
    ],
    'kimai'     => [
        // ADD YOUR CREDENTIALS
        'username'  => '',
        'password'  => '',

        // DO NOT CHANGE
        'endpoints' => [
            'base'  => 'http://timetracking.sixt.de/kimai',
            'login' => '/index.php',
            'main'  => '/core/kimai.php',
            'add'   => '/extensions/ki_timesheets/processor.php'
        ]
    ]
];
