
Kimai Importer
==============

#### Set up

1. Get you Office365 calendar url (image below shows how to publish your calendar) and pste in into config.php:

![Publish Calendar](http://i.imgur.com/PZEli6B.png "Publish Calendar")

2. Copy your Kimai credentials and paste into config.php

3. Set desired number of days to go back in time in config.php,  
for example to get calendar events for past 10 days the config.php  
settings would look like this([Relative datetime formats doc](http://php.net/manual/en/datetime.formats.relative.php "Relative datetime formats doc")): 

```
    'startDayOffset' => '-10 day',
    'endDayOffset' => '-1 day',
```
-------------

#### Run

- Run from console:
   
   ```artem@artem-Inspiron-7720:~/PhpstormProjects/blackops/KimaiImporter$ > php importToKimai.php```
   
- Run from cron (if your system supports `@reboot` keyword [Crontab doc](http://manpages.ubuntu.com/manpages/wily/man5/crontab.5.html "Crontab doc")):
    
```
    @reboot /usr/bin/php /home/artem/PhpstormProjects/blackops/KimaiImporter/importToKimai.php
```